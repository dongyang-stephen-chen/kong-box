import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { Header, TwoColumns, Paragraph, colorSchemeStyleSheets, typographyStyleSheet, Division, Hyperlink, KongBox } from '@chentech/kong-box'

export default {
  title: 'Two Columns',
  component: TwoColumns
} as ComponentMeta<typeof TwoColumns>

const twoColumns: ComponentStory<typeof TwoColumns> = (args) => <KongBox typographyStyleSheet={args.typographyStyleSheet} colorSchemeStyleSheets={args.colorSchemeStyleSheets}><TwoColumns header={args.header} main={args.main} left={args.left} footer={args.footer} ></TwoColumns></KongBox>
console.log(typographyStyleSheet(20,1.5))
export const lightmode = twoColumns.bind({})
lightmode.args = {
  colorSchemeStyleSheets: colorSchemeStyleSheets({
    background: "#eae7dc", 
    main:"#e85a4f", 
    subMain: "#e98074",  
    contrast: "#8e8d8a", 
    subcontrast:"#d8c3a5"}), 
  typographyStyleSheet: typographyStyleSheet(20,1.5),
  header: <Division>
    <Division><Header size={1}>Chentech Software</Header></Division>
      <Division padding>
        <Hyperlink  href="http://" >Home</Hyperlink>
        <Hyperlink  href="http://" >About</Hyperlink>
        <Hyperlink  href="http://" >Shop</Hyperlink>
      </Division>
    </Division>,
  main: <>
    <Header size={2}>Starship Web Design</Header>
    <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac nunc ac est convallis vehicula eget non massa. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed et molestie quam, ac luctus nisi. Maecenas tempor ipsum eu justo maximus, quis porttitor nunc posuere. In lobortis risus ut nisi varius convallis. Pellentesque quis lectus pretium, fermentum purus non, sodales purus. Morbi iaculis, nulla quis lobortis viverra, magna lectus euismod ligula, quis fermentum sem nunc ut tellus. Duis pellentesque laoreet augue cursus egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sit amet tincidunt mi, sit amet consequat turpis. Sed et lorem metus. Ut ultrices feugiat elit, non luctus purus molestie sed. Proin justo nisi, cursus in sem sed, vulputate sollicitudin ante. Praesent at erat pretium, lacinia tellus vitae, bibendum quam.</Paragraph>
    <Header size={3}>Starship Web Design</Header>
    <Paragraph >Nam semper vitae tortor sed rutrum. Nam feugiat elit nisi. Phasellus ac lorem id leo condimentum dictum. Cras sapien arcu, volutpat a tincidunt maximus, feugiat at elit. Morbi a elementum justo, eu malesuada velit. Ut aliquam purus accumsan orci consectetur iaculis. Morbi eget feugiat magna. Vivamus imperdiet ipsum semper elit posuere, sit amet faucibus libero vestibulum. Mauris lectus dolor, placerat vitae neque vitae, tincidunt elementum erat.</Paragraph>
    <Header size={4}>Starship Web Design</Header>
    <Paragraph >In sed gravida lacus. Phasellus tristique ipsum eu eros tristique, ut rutrum mi vulputate. Phasellus scelerisque sem ex, a auctor odio accumsan vitae. Morbi eros orci, condimentum at dui eget, consectetur placerat erat. In vitae massa erat. Aliquam convallis ultricies hendrerit. Quisque in libero quis odio interdum pellentesque at eget ante. Sed blandit lectus sagittis justo venenatis, quis ultricies lectus mattis. Mauris dictum diam non augue volutpat placerat. Pellentesque condimentum orci et nisi tincidunt accumsan.</Paragraph>
    <Header size={5} >Starship Web Design</Header>
    <Paragraph >Morbi faucibus, tellus quis rhoncus vulputate, felis orci elementum ligula, et pretium nulla dui eget dui. Etiam ultrices enim nec nisl porttitor vulputate. Aenean sit amet ultrices leo. Etiam eget aliquet erat. Maecenas vitae eros imperdiet, tincidunt sapien commodo, pellentesque nulla. Sed molestie suscipit sagittis. Aenean lobortis id nunc vel interdum. Suspendisse mattis neque id augue congue, a faucibus ipsum feugiat. Nam in sem mi.</Paragraph>
    <Header size={6} >Starship Web Design</Header>
    <Paragraph >Morbi faucibus, tellus quis rhoncus vulputate, felis orci elementum ligula, et pretium nulla dui eget dui. Etiam ultrices enim nec nisl porttitor vulputate. Aenean sit amet ultrices leo. Etiam eget aliquet erat. Maecenas vitae eros imperdiet, tincidunt sapien commodo, pellentesque nulla. Sed molestie suscipit sagittis. Aenean lobortis id nunc vel interdum. Suspendisse mattis neque id augue congue, a faucibus ipsum feugiat. Nam in sem mi.</Paragraph>
  </>,
  left: <Division margin={false}>
    <Hyperlink button={true} href="http://" >Starship Web Design</Hyperlink><br />
    <Hyperlink button={true} href="http://" >Celestial Scale</Hyperlink>
  </Division>,
  footer:
    <>
      <Paragraph >Suspendisse porttitor laoreet massa vitae laoreet. Vestibulum ligula turpis, malesuada ut est quis, placerat efficitur enim. Morbi fermentum turpis enim, convallis malesuada orci volutpat ut. Curabitur luctus urna nec ante ultricies semper. Maecenas at viverra nisi. Aliquam blandit justo dolor, dictum finibus ipsum fringilla nec. Morbi rhoncus orci vel accumsan suscipit. Nunc lacinia maximus tempus. Aliquam sollicitudin orci a massa sagittis, vitae luctus sapien tempus.</Paragraph>
    </>
}