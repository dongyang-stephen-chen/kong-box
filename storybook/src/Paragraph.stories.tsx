import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { Paragraph, typographyStyleSheet } from '@chentech/kong-box'

export default {
  title: 'Typography/Paragraph',
  component: Paragraph
} as ComponentMeta<typeof Paragraph>

const Template: ComponentStory<typeof Paragraph> = (args) => <Paragraph {...args} >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vel urna ut lacus molestie sagittis nec sit amet arcu. Maecenas vulputate felis pharetra felis pretium dignissim. Sed rutrum erat nec enim elementum, vitae porta erat gravida. Phasellus consequat rhoncus lorem, ac faucibus nulla scelerisque nec. Nulla ex lorem, commodo vitae massa at, tempor mattis enim. Phasellus elementum maximus diam non eleifend. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sit amet sagittis diam. Integer mattis accumsan tellus sit amet scelerisque. Donec dui turpis, pulvinar lacinia vulputate eget, consectetur eget libero.</Paragraph>

export const Baseline20 = Template.bind({})
Baseline20.args = {
  typographyStyleSheet: typographyStyleSheet(20, 1.5)
}


export const Baseline30 = Template.bind({})
Baseline30.args = {
  typographyStyleSheet: typographyStyleSheet(30, 1.5)
}
