import React, { FunctionComponent, PropsWithChildren } from 'react'
import { css } from 'aphrodite'
import { KongBoxContext } from './KongBox'
import { KongBoxProps } from '.'
type HyperLinkProps = KongBoxProps & {
    href: string
    button?: boolean,
    noStyle?: boolean
}


export const Hyperlink: FunctionComponent<HyperLinkProps> = ({ typographyStyleSheet, children, href, darkMode, colorSchemeStyleSheets, button = false, noStyle = false}: PropsWithChildren<HyperLinkProps>) => {

    return <KongBoxContext.Consumer>
        {context => {
            const usedTypographyStylesheet = typographyStyleSheet ?? context.typographyStyleSheet
            const usedcolorSchemeStyleSheets = colorSchemeStyleSheets ?? context.colorSchemeStyleSheets
            const usedDarkMode = darkMode ?? context.darkMode
            const className = css(
                usedTypographyStylesheet?.p,
                usedDarkMode ? usedcolorSchemeStyleSheets?.dark.typography.p : usedcolorSchemeStyleSheets?.light.typography.a,
                button ? usedTypographyStylesheet?.aButton : undefined,
                button ? usedcolorSchemeStyleSheets?.light.typography.aButton : undefined,
                noStyle ? usedTypographyStylesheet?.aNoStyle : undefined
            )
            return <a className={className}
                href={href}
            >
                {children}
            </a>
        }
        }
    </KongBoxContext.Consumer>
}