import React, { FunctionComponent, PropsWithChildren, useContext, useEffect, useState } from 'react'
import { Word, Clause, Sentence } from '@chentech/word-jam'
import { css, StyleSheet } from 'aphrodite'
import { KongBoxContext, KongBoxProps } from '.'

type WordJamProps = KongBoxProps & {
    rotate?: number,
    click?: boolean,
    content?: Word | Clause | Sentence
}
const styleSheet = StyleSheet.create({
    clickable: {
        ':hover': {
            cursor: 'pointer',
        },
    }
})
export const WordJam: FunctionComponent<WordJamProps> = ({
    content, click = false, rotate
}: PropsWithChildren<WordJamProps>) => {
    const [currentContent, setContent] = useState(content?.read())
    const clickHandler = () => {
        if (click) {
            setContent(content?.read())
        }
    }
    const {isRobot} = useContext(KongBoxContext)
    useEffect(() => {
        if (rotate && !isRobot) {
            const timer = setInterval(() => {
                setContent(content?.read());
            }, rotate);
            return () => { clearTimeout(timer) };
        }
    }, [rotate, isRobot])

    return <span onClick={clickHandler} className={css(styleSheet.clickable)}>
        {currentContent}
    </span>

}
