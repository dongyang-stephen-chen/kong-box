import React, { FunctionComponent, PropsWithChildren } from 'react'
import { css, StyleSheet } from 'aphrodite'
import { KongBoxContext, KongBoxProps } from '.'

type Props = KongBoxProps & {
    header?: React.ReactNode,
    main?: React.ReactNode,
    left?: React.ReactNode,
    right?: React.ReactNode,
    footer?: React.ReactNode,
}



export const HolyGrail: FunctionComponent<Props> = ({ header, footer, main, left, right, colorSchemeStyleSheets, darkMode }: PropsWithChildren<Props>) => {
    const styles = StyleSheet.create({
        container: {
            display: 'flex',
            flexDirection: 'column',

        },
        containerConditional: {
            flexDirection: 'row',
            flexWrap: 'wrap'
        },
        containerMediaQuery: {
            '@media all and (min-width: 1025px)': {
                flexDirection: 'row',
                flexWrap: 'wrap'
            }
        },
        headerFooterMediaQuery: {
            '@media all and (min-width: 1025px)': {
                width: '100%'
            }
        },
        headerFooterConditional: {
            width: '100%'
        },
        mainMediaQuery: {
            '@media all and (min-width: 1025px)': {
                flex: 2,
                order: 2,
                minHeight: '80vh'
            }
        },
        mainConditional: {
            flex: 2,
            order: 2,
            minHeight: '80vh'
        },
        leftMediaQuery: {
            '@media all and (min-width: 1025px)': {
                flex: 1,
                order: 1,
            }
        },
        leftConditional: {
            flex: 1,
            order: 1,
        },
        rightMediaQuery: {
            '@media all and (min-width: 1025px)': {
                flex: 1,
                order: 3,
            }
        },
        rightConditional: {
            flex: 1,
            order: 3,
        },
        footerMediaQuery: {
            '@media all and (min-width: 1025px)': {
                order: 4,
            }
        },
        footerConditional: {
            order: 4,
        }
    });

    return <KongBoxContext.Consumer>
        {context => {
            const usedcolorSchemeStyleSheets = colorSchemeStyleSheets ?? context.colorSchemeStyleSheets
            const usedDarkMode = darkMode ?? context.darkMode
            const contextHasWidth = !isNaN(context.kongBoxWidth)
            return <div className={css(
                styles.container,
                contextHasWidth ? (context.kongBoxWidth > 1025 ? styles.containerConditional : undefined) : styles.containerMediaQuery)
            }>
                <header className={
                    css(
                        contextHasWidth ? (context.kongBoxWidth > 1025 ? styles.headerFooterConditional : undefined) : styles.headerFooterMediaQuery,
                        usedDarkMode ? usedcolorSchemeStyleSheets?.dark.backgroundColor.middle : usedcolorSchemeStyleSheets?.light.backgroundColor.middle)
                }>{header}</header>

                <main className={css(
                    contextHasWidth ? (context.kongBoxWidth > 1025 ? styles.mainConditional : undefined) : styles.mainMediaQuery,
                    usedDarkMode ? usedcolorSchemeStyleSheets?.dark.backgroundColor.lighter : usedcolorSchemeStyleSheets?.light.backgroundColor.lighter
                )}>{main}</main>

                <aside className={css(
                    contextHasWidth ? (context.kongBoxWidth > 1025 ? styles.leftConditional : undefined) : styles.leftMediaQuery,
                    usedDarkMode ? usedcolorSchemeStyleSheets?.dark.backgroundColor.middle : usedcolorSchemeStyleSheets?.light.backgroundColor.middle
                )}>{left}</aside>

                <aside className={css(
                    contextHasWidth ? (context.kongBoxWidth > 1025 ? styles.rightConditional : undefined) : styles.rightMediaQuery,
                    usedDarkMode ? usedcolorSchemeStyleSheets?.dark.backgroundColor.middle : usedcolorSchemeStyleSheets?.light.backgroundColor.middle
                )}>{right}</aside>

                <footer className={css(
                    contextHasWidth ? (context.kongBoxWidth > 1025 ? styles.footerConditional : undefined) : styles.footerMediaQuery,
                    contextHasWidth ? (context.kongBoxWidth > 1025 ? styles.headerFooterConditional : undefined) : styles.headerFooterMediaQuery,
                    usedDarkMode ? usedcolorSchemeStyleSheets?.dark.backgroundColor.darker : usedcolorSchemeStyleSheets?.light.backgroundColor.darker)}>{footer}</footer>
            </div>
        }}
    </KongBoxContext.Consumer>
}