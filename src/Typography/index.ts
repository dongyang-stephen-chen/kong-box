export type TypographyConfiguration = {} & Partial<typeof defaultConfig>

const defaultConfig = {
    defaultFontSizeSeed: 20,
    lineHeightMultiplier: 1.5,
    sizeIncrement() { return this.defaultFontSizeSeed * this.lineHeightMultiplier },
    cssInJs(fontSize: number, margin: number, lineHeight: number) {
        return {
            fontFamily: 'Lucida Console',
            padding: 0,
            outline: 0,
            fontSize: fontSize + 'px',
            verticalAlign: 'baseline',
            background: 'transparent',
            lineHeight: lineHeight + 'px',
            margin: `${margin / 2}px ${margin / 2}px ${margin / 2}px 0`,
            marginBlockStart: `${margin / 2}px`,
            marginBlockEnd: '0',
            marginInlineStart: `${margin / 2}px`,
            marginInlineEnd: `${margin / 2}px`,
            paddingBlockStart: 0,
            paddingBlockEnd: 0,
            paddingInlineStart: 0,
            paddingInlineEnd: 0,
            boxSizing: 'border-box',
            quotes: 'none',
            ':before': {
                content: 'none'
            },
            ':after': {
                content: 'none'
            }
        }
    },
    bodyFontSize() { return this.defaultFontSizeSeed },
    bodyMargin() { return this.sizeIncrement() / 2 },
    bodyLineHeight() { return this.smallestLineHeight(this.bodyFontSize(), this.sizeIncrement()) },
    bodyCssInJs() { return this.cssInJs(this.bodyFontSize(), this.bodyMargin(), this.bodyLineHeight()) },
    pFontSize() { return this.defaultFontSizeSeed },
    pMargin() { return this.sizeIncrement() / 2 },
    pLineHeight() { return this.smallestLineHeight(this.pFontSize(), this.sizeIncrement()) },
    pCssInJs() { return this.cssInJs(this.pFontSize(), this.pMargin(), this.pLineHeight()) },
    h1FontSize() { return this.defaultFontSizeSeed * 3 },
    h1Margin() { return this.sizeIncrement() / 2 },
    h1LineHeight() { return this.smallestLineHeight(this.h1FontSize(), this.sizeIncrement()) },
    h1CssInJs() { return this.cssInJs(this.h1FontSize(), this.h1Margin(), this.h1LineHeight()) },
    h2FontSize() { return this.defaultFontSizeSeed * 2.25 },
    h2Margin() { return this.sizeIncrement() / 2 },
    h2LineHeight() { return this.smallestLineHeight(this.h2FontSize(), this.sizeIncrement()) },
    h2CssInJs() { return this.cssInJs(this.h2FontSize(), this.h2Margin(), this.h2LineHeight()) },
    h3FontSize() { return this.defaultFontSizeSeed * 1.75 },
    h3Margin() { return this.sizeIncrement() / 2 },
    h3LineHeight() { return this.smallestLineHeight(this.h3FontSize(), this.sizeIncrement()) },
    h3CssInJs() { return this.cssInJs(this.h3FontSize(), this.h3Margin(), this.h3LineHeight()) },
    h4FontSize() { return this.defaultFontSizeSeed * 1.25 },
    h4Margin() { return this.sizeIncrement() / 2 },
    h4LineHeight() { return this.smallestLineHeight(this.h4FontSize(), this.sizeIncrement()) },
    h4CssInJs() { return this.cssInJs(this.h4FontSize(), this.h4Margin(), this.h4LineHeight()) },
    h5FontSize() { return this.defaultFontSizeSeed * 1.0625 },
    h5Margin() { return this.sizeIncrement() / 2 },
    h5LineHeight() { return this.smallestLineHeight(this.h5FontSize(), this.sizeIncrement()) },
    h5CssInJs() { return this.cssInJs(this.h5FontSize(), this.h5Margin(), this.h5LineHeight()) },
    h6FontSize() { return this.defaultFontSizeSeed },
    h6Margin() { return this.sizeIncrement() / 2 },
    h6LineHeight() { return this.smallestLineHeight(this.h6FontSize(), this.sizeIncrement()) },
    h6CssInJs() { return this.cssInJs(this.h6FontSize(), this.h6Margin(), this.h6LineHeight()) },
    blockQuoteFontSize() { return this.defaultFontSizeSeed },
    blockQuoteMargin() { return this.sizeIncrement() / 2 },
    blockQuoteLineHeight() { return this.smallestLineHeight(this.blockQuoteFontSize(), this.sizeIncrement()) },
    blockQuoteCssInJs() { return this.cssInJs(this.blockQuoteFontSize(), this.blockQuoteMargin(), this.blockQuoteLineHeight()) },
    smallestLineHeight(fontsize: number, sizeIncrement: number): number {
        let x = 1
        while (sizeIncrement * x < fontsize) {
            x++
        }
        return sizeIncrement * x
    },
}

const configRunner = function (override?: TypographyConfiguration) {
    const config = { ...defaultConfig, ...override }
    return {
        body: config.bodyCssInJs(),
        p: config.pCssInJs(),
        pIndent: { textIndent: `${config.pMargin()}px` },
        h1: config.h1CssInJs(),
        h2: config.h2CssInJs(),
        h3: config.h3CssInJs(),
        h4: config.h4CssInJs(),
        h5: config.h5CssInJs(),
        h6: config.h6CssInJs(),
        blockQuote: config.blockQuoteCssInJs(),
        margin: { margin: `${config.sizeIncrement() / 2}px` },
        padding: { padding: `${config.sizeIncrement() / 2}px` },
        aNoStyle: {
            textDecoration: 'none'
        }
    }
}
