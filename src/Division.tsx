import React, { FunctionComponent, PropsWithChildren } from 'react'
import { css } from 'aphrodite'
import { KongBoxContext } from './KongBox'
import { KongBoxProps } from '.'

type DivisionProps = KongBoxProps & {
    margin?: boolean,
    padding?: boolean,
}

export const Division: FunctionComponent<DivisionProps> = ({ typographyStyleSheet, children, darkMode, colorSchemeStyleSheets, margin = false, padding = false }: PropsWithChildren<DivisionProps>) =>
    <KongBoxContext.Consumer>
        {context => {
            const usedTypographyStylesheet = typographyStyleSheet ?? context.typographyStyleSheet
            const usedcolorSchemeStyleSheets = colorSchemeStyleSheets ?? context.colorSchemeStyleSheets
            const usedDarkMode = darkMode ?? context.darkMode
            return <div className={css(
                usedDarkMode ? usedcolorSchemeStyleSheets?.dark.typography.p : usedcolorSchemeStyleSheets?.light.typography.p,
                margin ? usedTypographyStylesheet?.margin : undefined,
                padding ? usedTypographyStylesheet?.padding : undefined
            )}>
                {children}
            </div>
        }
        }
    </KongBoxContext.Consumer>
