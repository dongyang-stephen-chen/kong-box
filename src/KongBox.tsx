import React, { useState, useEffect } from "react";
import { FunctionComponent } from "react";
import { colorSchemeCssToAphroditeStylesheet, typographyCssToAphroditeStylesheet } from "./aphroditeConvertor";

export type KongBoxProps = {
    typographyStyleSheet?: ReturnType<typeof typographyCssToAphroditeStylesheet>,
    colorSchemeStyleSheets?: ReturnType<typeof colorSchemeCssToAphroditeStylesheet>
    darkMode?: boolean
    children?: any
}
export const KongBoxContext = React.createContext<{
    typographyStyleSheet?: ReturnType<typeof typographyCssToAphroditeStylesheet>;
    colorSchemeStyleSheets?: ReturnType<typeof colorSchemeCssToAphroditeStylesheet>
    kongBoxWidth: number,
    darkMode?: boolean,
    isRobot: boolean
}>({
    kongBoxWidth: NaN,
    typographyStyleSheet: undefined,
    colorSchemeStyleSheets: undefined,
    darkMode: false,
    isRobot: false
});

const detectRobot = (userAgent: string): boolean => {
    const robots = new RegExp(([
      /bot/,/spider/,/crawl/,                               // GENERAL TERMS
      /APIs-Google/,/AdsBot/,/Googlebot/,                   // GOOGLE ROBOTS
      /mediapartners/,/Google Favicon/,
      /FeedFetcher/,/Google-Read-Aloud/,
      /DuplexWeb-Google/,/googleweblight/,
      /bing/,/yandex/,/baidu/,/duckduck/,/yahoo/,           // OTHER ENGINES
      /ecosia/,/ia_archiver/,
      /facebook/,/instagram/,/pinterest/,/reddit/,          // SOCIAL MEDIA
      /slack/,/twitter/,/whatsapp/,/youtube/,
      /semrush/,                                            // OTHER
    ] as RegExp[]).map((r) => r.source).join("|"),"i");     // BUILD REGEXP + "i" FLAG
  
    return robots.test(userAgent);
  };

export const KongBox: FunctionComponent<KongBoxProps & { trackWidth?: boolean }> = ({ typographyStyleSheet, trackWidth = false, children, darkMode, colorSchemeStyleSheets }) => {
    const [kongBoxWidth, setWidth] = useState(NaN)
    const divRef = React.useRef<HTMLDivElement>(null);
    const userAgent = navigator.userAgent;
    const isRobot = detectRobot(userAgent);
    if (trackWidth) {
        useEffect(() => {
            const handleResize = (e: Event) => {
                setWidth(divRef?.current?.offsetWidth ?? NaN)
            }
            window.addEventListener("resize", handleResize);
            return () => window.removeEventListener("resize", handleResize)
        })
    }
    console.log(colorSchemeStyleSheets)
    return <div ref={divRef}>
        <KongBoxContext.Provider value={{ kongBoxWidth, typographyStyleSheet, darkMode, colorSchemeStyleSheets, isRobot: isRobot }}>
            {children}
        </KongBoxContext.Provider>
    </div>
}