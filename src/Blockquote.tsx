import React, { FunctionComponent, PropsWithChildren } from 'react'
import { css } from 'aphrodite'
import { typographyCssToAphroditeStylesheet } from './aphroditeConvertor'

type BlockquoteProps = {
  styleSheet: ReturnType<typeof typographyCssToAphroditeStylesheet>
}

export const Blockquote: FunctionComponent<BlockquoteProps> = ({ styleSheet: simpleImplementation, children }: PropsWithChildren<BlockquoteProps>) => <blockquote className={css(simpleImplementation.blockQuote)}>{children}</blockquote>
