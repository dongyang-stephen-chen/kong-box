import { calculateTypography, typographyCss } from './verticalRhythm'
import { CSSProperties, StyleSheet } from 'aphrodite'
import { calculateColorScheme } from './ColorScheme'

function defaultTypographyStyles(implementation: typographyCss): CSSProperties {
  const { margin } = implementation
  return {
    fontFamily: 'Lucida Console',
    padding: 0,
    outline: 0,
    fontSize: implementation.fontSize + 'px',
    verticalAlign: 'baseline',
    background: 'transparent',
    lineHeight: implementation.lineHeight + 'px',
    margin: `${margin/2}px ${margin/2}px ${margin/2}px 0`,
    marginBlockStart: `${margin/2}px`,
    marginBlockEnd: '0',
    marginInlineStart: `${margin/2}px`,
    marginInlineEnd: `${margin/2}px`,
    paddingBlockStart: 0,
    paddingBlockEnd: 0,
    paddingInlineStart: 0,
    paddingInlineEnd: 0,
    boxSizing: 'border-box',
    quotes: 'none',
    ':before': {
      content: 'none'
    },
    ':after': {
      content: 'none'
    }
  }
}

export function typographyCssToAphroditeStylesheet(implementation: ReturnType<typeof calculateTypography>) {
  return StyleSheet.create({
    body: defaultTypographyStyles(implementation.body),
    p: defaultTypographyStyles(implementation.p),
    pIndent: { textIndent: `${implementation.p.margin}px` },
    h1: defaultTypographyStyles(implementation.h1),
    h2: defaultTypographyStyles(implementation.h2),
    h3: defaultTypographyStyles(implementation.h3),
    h4: defaultTypographyStyles(implementation.h4),
    h5: defaultTypographyStyles(implementation.h5),
    h6: defaultTypographyStyles(implementation.h6),
    blockQuote: defaultTypographyStyles(implementation.blockQuote),
    margin: { margin: `${implementation.baseLine}px` },
    padding: { padding: `${implementation.baseLine}px` },
    aButton: {
      padding: `${implementation.baseLine / 2 }px ${implementation.baseLine}px`,
      borderRadius: `${3}px`,
    },
    aNoStyle: {
      textDecoration: 'none'
    }
  })
}

function fullBorderTextShadow(thickness: number, blur: number, color: string) {
  return { textShadow: `-${thickness}px 0 ${blur}px ${color}, 0 ${thickness}px ${blur}px ${color}, ${thickness}px 0 ${blur}px ${color}, 0 -${thickness}px ${blur}px ${color}` }
}

export function colorSchemeCssToAphroditeStylesheet(implementation: ReturnType<typeof calculateColorScheme>) {
  console.log(implementation)
  return {
    light: {
      typography: StyleSheet.create({
        p: { color: implementation.background.textOverlay },
        h1: { color: implementation.main.middle, borderBottom: `${implementation.subcontrast.middle} solid 3px`, ...fullBorderTextShadow(1, 0, implementation.contrast.middle) },
        h2: { color: implementation.main.middle, borderBottom: `${implementation.subcontrast.middle} solid 2px`, ...fullBorderTextShadow(1, 0, implementation.contrast.middle) },
        h3: { color: implementation.main.middle, borderBottom: `${implementation.subcontrast.middle} solid 1px`, textShadow: `1px 1px 1px ${implementation.contrast.middle}` },
        h4: { color: implementation.main.middle, textShadow: `1px 1px ${implementation.contrast.middle}` },
        h5: { color: implementation.main.middle, textShadow: `0px 1px ${implementation.contrast.middle}` },
        h6: { color: implementation.main.middle, textShadow: `0px 1px ${implementation.contrast.middle}` },
        a: { color: implementation.subMain.middle },
        aButton: {
          backgroundColor: implementation.subMain.middle,
          borderBottom: `${implementation.contrast.middle} solid 3px`,
          color: implementation.subMain.textOverlay,
          textDecoration: 'none',
          textTransform: 'uppercase',
          position: 'relative',
          display: 'inline-block',
          ':hover': {
            backgroundColor: implementation.subMain.lighter,
            cursor: 'pointer'
          },
          ':active': {
            borderBottom: 'none',
            top: '3px',
            marginBottom: '3px'
          },
          textAlign: 'center',
          textShadow: `1px 1px ${implementation.subcontrast.black}`
        }
      }),
      backgroundColor: StyleSheet.create({
        lighter: { backgroundColor: implementation.background.lighter },
        middle: { backgroundColor: implementation.background.middle },
        darker: { backgroundColor: implementation.background.darker }
      })
    },
    dark: {
      typography: StyleSheet.create({
        p: { color: implementation.background.textOverlay },
        h1: { color: implementation.main.middle, ...fullBorderTextShadow(1, 0, implementation.contrast.middle) },
        h2: { color: implementation.main.middle, ...fullBorderTextShadow(1, 0, implementation.contrast.middle) },
        h3: { color: implementation.main.middle, textShadow: `1px 1px 1px ${implementation.contrast.middle}` },
        h4: { color: implementation.main.middle, textShadow: `1px 1px ${implementation.contrast.middle}` },
        h5: { color: implementation.main.middle, textShadow: `0px 1px ${implementation.contrast.middle}` },
        h6: { color: implementation.main.middle, textShadow: `0px 1px ${implementation.contrast.middle}` },
        a: { color: implementation.subMain.middle },
        aButton: {
          backgroundColor: implementation.subMain.middle,
          borderBottom: `${implementation.contrast.middle} solid 3px`,
          color: implementation.subMain.textOverlay,
          textDecoration: 'none',
          textTransform: 'uppercase',
          position: 'relative',
          display: 'inline-block',
          ':hover': {
            backgroundColor: implementation.subMain.lighter,
            cursor: 'pointer'
          },
          ':active': {
            borderBottom: 'none',
            top: '3px',
            marginBottom: '3px'
          },
          textAlign: 'center',
          textShadow: `1px 1px ${implementation.subcontrast.black}`
        }
      }),
      backgroundColor: StyleSheet.create({
        lighter: { backgroundColor: implementation.background.lighter },
        middle: { backgroundColor: implementation.background.middle },
        darker: { backgroundColor: implementation.background.darker }
      })
    }
  }
}