import { colorSchemeCssToAphroditeStylesheet, typographyCssToAphroditeStylesheet } from './aphroditeConvertor'
import { calculateColorScheme, ColorSchemeParams } from './ColorScheme'
import { calculateTypography } from './verticalRhythm'

export function typographyStyleSheet(fontSize: number, lineHeight: number) {
  return typographyCssToAphroditeStylesheet(calculateTypography({ fontSize, lineHeight }))
}

export function colorSchemeStyleSheets(params: ColorSchemeParams) {
  return colorSchemeCssToAphroditeStylesheet(calculateColorScheme(params))
}