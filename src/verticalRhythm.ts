interface bodyTypographyCss {
  /**
   * font-size in pixels
   */
  fontSize: number
  /**
   * line-height as multiplication of font-size
   */
  lineHeight: number
}

/**
 *
 * @param textCss
 * @returns line height in pixels
 */
export function calculateBaseLine(textCss: bodyTypographyCss): number {
  return textCss.fontSize * textCss.lineHeight
}

export interface typographyCss extends bodyTypographyCss {
  margin: number
}

export function getSmallestLineHeight(fontsize: number, baseline: number): number {
  let x = 1
  while (baseline * x < fontsize) {
    x++
  }
  return baseline * x
}

function createStyles(fontsize: number, baseline: number) {
  return {
    fontSize: fontsize,
    lineHeight: getSmallestLineHeight(fontsize, baseline),
    margin: baseline
  }
}

export function calculateTypography(textCss: bodyTypographyCss) {
  const baseLine = calculateBaseLine(textCss)
  return {
    body: createStyles(textCss.fontSize, baseLine),
    p: createStyles(textCss.fontSize, baseLine),
    h1: createStyles(textCss.fontSize * 3, baseLine),
    h2: createStyles(textCss.fontSize * 2.25, baseLine),
    h3: createStyles(textCss.fontSize * 1.75, baseLine),
    h4: createStyles(textCss.fontSize * 1.25, baseLine),
    h5: createStyles(textCss.fontSize * 1.0625, baseLine),
    h6: createStyles(textCss.fontSize, baseLine),
    blockQuote: createStyles(textCss.fontSize * 1.5, baseLine),
    baseLine: baseLine
  }
}
