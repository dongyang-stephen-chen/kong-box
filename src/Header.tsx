import React, { FunctionComponent, PropsWithChildren } from 'react'
import { css } from 'aphrodite'
import { typographyCssToAphroditeStylesheet } from './aphroditeConvertor'
import { KongBoxContext, KongBoxProps } from '.'

type HeaderProps = KongBoxProps & {
  size: 1 | 2 | 3 | 4 | 5 | 6
}

export const Header: FunctionComponent<HeaderProps> = ({ typographyStyleSheet, children, size, colorSchemeStyleSheets, darkMode }: PropsWithChildren<HeaderProps>) =>
  <KongBoxContext.Consumer>
    {context => {
      const usedTypographyStylesheet = typographyStyleSheet ?? context.typographyStyleSheet
      const usedcolorSchemeStyleSheets = colorSchemeStyleSheets ?? context.colorSchemeStyleSheets
      const usedDarkMode = darkMode ?? context.darkMode

      switch (size) {
        case 1:
          return <h1 className={css(
            usedTypographyStylesheet?.h1,
            usedDarkMode ? usedcolorSchemeStyleSheets?.dark.typography[`h${size}`] : usedcolorSchemeStyleSheets?.light.typography[`h${size}`]
          )}>{children}</h1>
        case 2:
          return <h2 className={css(
            usedTypographyStylesheet?.h2,
            usedDarkMode ? usedcolorSchemeStyleSheets?.dark.typography[`h${size}`] : usedcolorSchemeStyleSheets?.light.typography[`h${size}`]
          )}>{children}</h2>
        case 3:
          return <h3 className={css(
            usedTypographyStylesheet?.h3,
            usedDarkMode ? usedcolorSchemeStyleSheets?.dark.typography[`h${size}`] : usedcolorSchemeStyleSheets?.light.typography[`h${size}`]
          )}>{children}</h3>
        case 4:
          return <h4 className={css(
            usedTypographyStylesheet?.h4,
            usedDarkMode ? usedcolorSchemeStyleSheets?.dark.typography[`h${size}`] : usedcolorSchemeStyleSheets?.light.typography[`h${size}`]
          )}>{children}</h4>
        case 5:
          return <h5 className={css(
            usedTypographyStylesheet?.h5,
            usedDarkMode ? usedcolorSchemeStyleSheets?.dark.typography[`h${size}`] : usedcolorSchemeStyleSheets?.light.typography[`h${size}`]
          )}>{children}</h5>
        case 6:
          return <h6 className={css(
            usedTypographyStylesheet?.h6,
            usedDarkMode ? usedcolorSchemeStyleSheets?.dark.typography[`h${size}`] : usedcolorSchemeStyleSheets?.light.typography[`h${size}`]
          )}>{children}</h6>
      }
    }}
  </KongBoxContext.Consumer>

