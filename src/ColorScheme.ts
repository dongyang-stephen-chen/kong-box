import convertor from 'color-convert'
function getContrastYIQ(hexcolor: string) {
    var r = parseInt(hexcolor.substring(0, 2), 16);
    var g = parseInt(hexcolor.substring(2, 4), 16);
    var b = parseInt(hexcolor.substring(4, 6), 16);
    console.log({r,g,b})
    var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
    return (yiq >= 128) ? 'black' : 'white';
}
export interface ColorSchemeParams { main: string, subMain: string, contrast: string, subcontrast: string, background: string }

function schemeSegment(color: string){

    const HSL = convertor.hex.hsl(color)
    const INT = parseInt(color.slice(1), 16)
    const oppositeINT = 0xFFFFFF - INT
    const contrast = "#"+ oppositeINT.toString(16).padStart(6, '0')
    const contrastHSL = convertor.hex.hsl(contrast)
    const black = '#' + convertor.hsl.hex([contrastHSL[0], contrastHSL[1], 20])
    const white = '#' + convertor.hsl.hex([contrastHSL[0], contrastHSL[1], 80]) 
    let lighterLumin = HSL[2] == 100 ? 100 : HSL[2]  +1
    let darkerLumin = HSL[2] == 0 ? 0 : HSL[2] -1
    console.log(HSL[2], {lighterLumin, darkerLumin})
    return { 
        middle: color,
        lighter: '#' + convertor.hsl.hex([HSL[0], HSL[1], lighterLumin]),
        darker: '#' + convertor.hsl.hex([HSL[0], HSL[1], darkerLumin]),
        contrast: contrast,
        textOverlay: getContrastYIQ(color.slice(1)) == 'white' ? white : black,
        black: black,
        white: white,
    }
}
export const calculateColorScheme = (params: ColorSchemeParams) => {
    const { main, subMain, contrast, subcontrast, background } = params

    return {
        main: schemeSegment(main),
        subMain: schemeSegment(subMain),
        contrast: schemeSegment(contrast),
        subcontrast: schemeSegment(subcontrast),
        background: schemeSegment(background),
    }
}