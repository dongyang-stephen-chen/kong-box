import React, { FunctionComponent, PropsWithChildren } from 'react'
import { css } from 'aphrodite'
import { KongBoxContext } from './KongBox'
import { KongBoxProps } from '.'

type ParagraphProps = KongBoxProps & {
  noIndent?: boolean
}

export const Paragraph: FunctionComponent<ParagraphProps> = ({ typographyStyleSheet, children, noIndent = false, darkMode, colorSchemeStyleSheets }: PropsWithChildren<ParagraphProps>) =>
  <KongBoxContext.Consumer>
    {context => {
      const usedTypographyStylesheet = typographyStyleSheet ?? context.typographyStyleSheet
      const usedcolorSchemeStyleSheets = colorSchemeStyleSheets ?? context.colorSchemeStyleSheets
      const usedDarkMode = darkMode ?? context.darkMode
      return <p className={css(
        usedTypographyStylesheet?.p,
        noIndent ? undefined : usedTypographyStylesheet?.pIndent,
        usedDarkMode ?  usedcolorSchemeStyleSheets?.dark.typography.p : usedcolorSchemeStyleSheets?.light.typography.p
      )}>
        {children}
      </p>
    }
    }
  </KongBoxContext.Consumer>
